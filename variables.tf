variable "repository_name" {
  type        = string
  description = "Name of the repository"
  default     = ""
}
variable "image_tag_mutability" {
  description = "The tag mutability setting for the repository. Must be one of: `MUTABLE` or `IMMUTABLE`."
  type        = string
  default     = "MUTABLE"
}
variable "tags" {
  description = "A mapping of tags to assign to the resource."
  type        = map(string)
  default     = {}
}

variable "max_image_count" {
  type        = number
  description = "How many image AWS ECR will store"
  default     = 15
}


