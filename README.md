# AWS ECR terraform module
Terraform module, which will create new ecr repository.

## Objects created in Amazon ECR repositories after deployment

- **Private repository** - *name_repository* 

The policy has 2 rules: to remove untagged images and to delete old images.

## Basic usage
```terraform

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.66.0"
    }
  }
}

provider "aws" {
  region = "eu-west-1"
  access_key = "xxxx"
  secret_key = "xxxx"
}

\\
module "ecr" {
  source = "./modules/terraform-module-aws-ecr"

  for_each             = var.repositories
  repository_name      = each.value.name
  image_tag_mutability = each.value.image_tag_mutability
  tags                 = each.value.tags
  max_image_count      = each.value.max_image_count

  providers = {
    aws = aws.eu
  }
}
```
